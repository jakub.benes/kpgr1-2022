import rasterize.LineRasterizer;
import rasterize.LineRasterizerGraphics;
import rasterize.RasterBufferedImage;
import rasterize.TrivialLineRasterizer;
import render.WireRenderer;
import solids.Cube;
import solids.Solid;
import transforms.Mat4;
import transforms.Mat4Transl;
import transforms.Point3D;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Vlastní examples.Canvas, který využívá RasterBufferedImage a LineRasterizer.
 */
public class Controller3D {
    private JPanel panel;
    private RasterBufferedImage raster;
    private LineRasterizer lineRasterizer;

    // TODO: Camera .. třída Camera
    // .getViewMatrix()

    public Controller3D(int width, int height) {
        // Instance okna
        JFrame frame = new JFrame();
        // Layout okna
        frame.setLayout(new BorderLayout());
        // Titulek okna
        frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
        // Není možné měnit velikost okna
        frame.setResizable(false);
        // Pokud by zde nebylo, po zavření okna by se neukončila aplikace, důležité!
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Instance rasteru. Můžeme mít různé rastery.
        raster = new RasterBufferedImage(width, height);
        // Instance line rasterizeru. Můžeme mít různé rasterizery. Např. pro každý algoritmus tzn. TrivialLineRasterizer, MidpointLineRasterizer, ...
        lineRasterizer = new LineRasterizerGraphics(raster);
        lineRasterizer.setColor(0xffff00);

        // Kamera
        // jsem v y = -3, dívám se do +y
//        camera = new Camera(
//                new Vec3D(0., -3., 0.), // position
//                Math.toRadians(90), // azimuth
//                Math.toRadians(0), // zenith
//                3, true
//        );

        // Vytvoříme panel, který umí vykreslit BufferedImage
        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                // Vykreslíme BufferedImage
                present(g);
            }
        };
        // Velikost panelu
        panel.setPreferredSize(new Dimension(width, height));

        // Panel přidáme do okna, okno zvětšíme na jeho obsah, okno nastavíme jako viditelné
        frame.add(panel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

    }

    private void display() {
        clear(0xaaaaaa);

        Solid cube = new Cube();
        // Matice posunutí
        Mat4 transl = new Mat4Transl(0.2, 0.2, 0);
        cube.setModel(transl);

        Solid cube2 = new Cube();

        List<Solid> scene = new ArrayList<>();
        scene.add(cube);
        scene.add(cube2);

        WireRenderer wireRenderer = new WireRenderer(lineRasterizer);
        wireRenderer.renderScene(scene);

        panel.repaint();
    }

    // Pomocí Graphics vykreslí raster do panelu. Specifické pro BufferedImage
    private void present(Graphics graphics) {
        raster.repaint(graphics);
    }

    // Vyčistí raster danou barvou
    private void clear(int color) {
        raster.setClearColor(color);
        raster.clear();
    }

    // Při spuštění provedeme vyčištění rasteru a raster zobrazíme
    public void start() {
        display();
    }


}
