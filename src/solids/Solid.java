package solids;

import transforms.Mat4;
import transforms.Mat4Identity;
import transforms.Point3D;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Solid {
    // Seznam vrcholů - geometrie
    protected List<Point3D> vertexBuffer = new ArrayList<>();
    // Seznam indexů - topologie
    protected List<Integer> indexBuffer = new ArrayList<>();

    // Modelovací matice (posun, rotace, scale)
    private Mat4 model = new Mat4Identity();

    protected void addIndicies(Integer... indices) {
        indexBuffer.addAll(Arrays.asList(indices));
    }

    public List<Point3D> getVertexBuffer() {
        return vertexBuffer;
    }

    public List<Integer> getIndexBuffer() {
        return indexBuffer;
    }

    public Mat4 getModel() {
        return model;
    }

    public void setModel(Mat4 model) {
        this.model = model;
    }
}
