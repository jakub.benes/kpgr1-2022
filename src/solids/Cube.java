package solids;

import transforms.Point3D;

public class Cube extends Solid {

    public Cube() {
        // Vrcholy
        vertexBuffer.add(new Point3D(-1, -1, 1));
        vertexBuffer.add(new Point3D( 1, -1, 1));
        vertexBuffer.add(new Point3D( 1,  1, 1));
        vertexBuffer.add(new Point3D(-1,  1, 1));

        // Hrany
        addIndicies(0, 1, 1, 2, 2, 3, 3, 0);
    }
}
