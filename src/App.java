import javax.swing.*;

public class App {
    // Vstupní bod do aplikace
    public static void main(String[] args) {
        // Vytvoří se instance třídy MyCanvas a zavolá se metoda start()
        SwingUtilities.invokeLater(() -> new Controller3D(800, 600).start());
    }
}
