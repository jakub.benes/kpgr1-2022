package render;

import rasterize.LineRasterizer;
import solids.Solid;
import transforms.Point3D;
import transforms.Vec3D;

import java.awt.*;
import java.util.List;

public class WireRenderer {

    private LineRasterizer lineRasterizer;
    // private Mat4 View matice
    // private proj - projekce

    public WireRenderer(LineRasterizer lineRasterizer) {
        this.lineRasterizer = lineRasterizer;
    }

    public void renderScene(List<Solid> scene) {
        for (Solid solid:scene) {
            renderSolid(solid);
        }
    }

    public void renderSolid(Solid solid) {
        for (int i = 0; i < solid.getIndexBuffer().size(); i += 2) {
            int index1 = solid.getIndexBuffer().get(i);
            int index2 = solid.getIndexBuffer().get(i + 1);

            Point3D p1 = solid.getVertexBuffer().get(index1);
            Point3D p2 = solid.getVertexBuffer().get(index2);

            p1 = p1.mul(solid.getModel());
            p2 = p2.mul(solid.getModel());
            // TODO: Přinásobit view
            // TODO: přinásobit proj

            // TODO: ořezání = Fast clip
            // −𝑤 ≤ 𝑥 ≤ 𝑤 ,−𝑤 ≤ 𝑦 ≤ 𝑤 a 0 ≤ 𝑧 ≤ w
            // if(-p1.getW() <= p1.getX() && p1.getX() <= p1.getW() && )

            // TODO: dehomogenizace

            Vec3D v1 = transformToWindow(p1);
            Vec3D v2 = transformToWindow(p2);

            lineRasterizer.rasterize(
                    (int)Math.round(v1.getX()),
                    (int)Math.round(v1.getY()),
                    (int)Math.round(v2.getX()),
                    (int)Math.round(v2.getY()),
                    Color.YELLOW
            );
        }
    }

    private Vec3D transformToWindow(Point3D p) {
        Vec3D v = new Vec3D(p);
        v = v.mul(new Vec3D(1, -1, 1))
            .add(new Vec3D(1, 1, 0))
            .mul(new Vec3D((800 - 1) /2, (600 - 1) / 2, 1));

        return v;
    }
}
